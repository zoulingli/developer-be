package org.edgegallery.developer.model.appstore;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PublishAppErrResponse {

    private int errCode;

    private String message;

    private List<String> params;
}
