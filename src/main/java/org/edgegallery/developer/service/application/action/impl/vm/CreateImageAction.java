/*
 *    Copyright 2021 Huawei Technologies Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.edgegallery.developer.service.application.action.impl.vm;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.edgegallery.developer.common.Consts;
import org.edgegallery.developer.model.application.Application;
import org.edgegallery.developer.model.instantiate.vm.EnumImageExportStatus;
import org.edgegallery.developer.model.instantiate.vm.ImageExportInfo;
import org.edgegallery.developer.model.lcm.LcmLog;
import org.edgegallery.developer.model.lcm.MepmVmImageInfo;
import org.edgegallery.developer.model.meao.ThirdSystem;
import org.edgegallery.developer.model.operation.ActionStatus;
import org.edgegallery.developer.model.operation.EnumActionStatus;
import org.edgegallery.developer.model.operation.EnumOperationObjectType;
import org.edgegallery.developer.model.resource.mephost.MepHost;
import org.edgegallery.developer.service.application.ApplicationService;
import org.edgegallery.developer.service.application.action.impl.AbstractAction;
import org.edgegallery.developer.service.application.common.EnumCreateImageStatus;
import org.edgegallery.developer.service.application.common.EnumExportImageStatus;
import org.edgegallery.developer.service.application.common.IContextParameter;
import org.edgegallery.developer.service.application.impl.vm.VMAppOperationServiceImpl;
import org.edgegallery.developer.service.application.vm.VMAppVmService;
import org.edgegallery.developer.service.recource.mephost.MepHostService;
import org.edgegallery.developer.util.HttpClientUtil;
import org.edgegallery.developer.util.SpringContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

public class CreateImageAction extends AbstractAction {

    public static final Logger LOGGER = LoggerFactory.getLogger(CreateImageAction.class);

    private static Gson gson = new Gson();

    public static final String ACTION_NAME = "Create Image";

    // time out: 180 min.
    public static final int TIMEOUT = 180 * 60 * 1000;

    // time out: 10 min.
    public static final int MEAO_TIME_OUT = 10 * 60 * 1000;

    //interval of the query, 5s.
    public static final int INTERVAL = 20000;

    ApplicationService applicationService = (ApplicationService) SpringContextUtil.getBean(ApplicationService.class);

    MepHostService mepHostService = (MepHostService) SpringContextUtil.getBean(MepHostService.class);

    VMAppOperationServiceImpl vmAppOperationService = (VMAppOperationServiceImpl) SpringContextUtil
        .getBean(VMAppOperationServiceImpl.class);

    VMAppVmService vmAppVmService = (VMAppVmService) SpringContextUtil.getBean(VMAppVmService.class);

    @Override
    public String getActionName() {
        return ACTION_NAME;
    }

    @Override
    public boolean execute() {
        //Start action , save action status.
        String vmId = (String) getContext().getParameter(IContextParameter.PARAM_VM_ID);
        String applicationId = (String) getContext().getParameter(IContextParameter.PARAM_APPLICATION_ID);
        Application application = applicationService.getApplication(applicationId);
        String statusLog = "Start to create vm image for vm Id：" + vmId;
        LOGGER.info(statusLog);
        ActionStatus actionStatus = initActionStatus(EnumOperationObjectType.VM_IMAGE_INSTANCE, vmId, ACTION_NAME,
            statusLog);
        String mepHostId = application.getMepHostId();
        if (null == mepHostId || "".equals(mepHostId)) {
            actionStatus.setStatus(EnumActionStatus.FAILED);
            updateActionError(actionStatus, "Sandbox not selected. Failed to instantiate package");
            return false;
        }
        String imageName = vmAppVmService.getVm(applicationId, vmId).getName();
        LOGGER.info("imageName:{}", imageName);
        MepHost mepHost = mepHostService.getHost(mepHostId);

        List<ThirdSystem> thirdSystems = HttpClientUtil.getThirdSystems();
        LOGGER.info("thirdSystems:{}", thirdSystems);
        boolean ret;
        if (!CollectionUtils.isEmpty(thirdSystems)) {
            ret = createImageFromMeao(actionStatus, thirdSystems, mepHost, imageName);
        } else {
            ret = createImageFromLcm(actionStatus, mepHost, imageName);
        }
        return ret;
    }

    private boolean createImageFromLcm(ActionStatus actionStatus, MepHost mepHost, String imageName) {
        //create image.
        LcmLog lcmLog = new LcmLog();
        String imageId = sentCreateImageRequestToLcm(mepHost, imageName, lcmLog);
        if (null == imageId) {
            String msg = "create vm  image  failed. The log from lcm is : " + lcmLog.getLog();
            updateActionError(actionStatus, msg);
            return false;
        }
        String msg = "create vm  image request sent to lcm controller success. imageId is: " + imageId;
        updateActionProgress(actionStatus, 30, msg);
        //Save  imageId to ImageExportInfo.
        boolean updateRes = saveImageIdToImageExportInfo(imageId);
        if (!updateRes) {
            updateActionError(actionStatus, "Update ImageId To image export info failed.");
            return false;
        }
        getContext().addParameter(IContextParameter.PARAM_IMAGE_INSTANCE_ID, imageId);
        // get vm export image status
        EnumExportImageStatus exportImageStatus = queryImageStatus(mepHost, imageId);
        if (!EnumExportImageStatus.COMPRESS_IMAGE_STATUS_SUCCESS.equals(exportImageStatus)) {
            String imageErrorLog = "Query export image status failed, the result is: " + exportImageStatus;
            updateActionError(actionStatus, imageErrorLog);
            modifyImageExportInfo(EnumImageExportStatus.FAILED, imageErrorLog);
            sendDeleteVmImageToLcm(mepHost);
            return false;
        }
        actionStatus.setStatus(EnumActionStatus.SUCCESS);
        updateActionProgress(actionStatus, 100, "Query export image status success.");
        modifyImageExportInfo(EnumImageExportStatus.IMAGE_QUERYING, "Query export image status success.");
        return true;
    }

    private boolean createImageFromMeao(ActionStatus actionStatus, List<ThirdSystem> thirdSystems, MepHost mepHost,
        String imageName) {
        ThirdSystem thirdSystem = thirdSystems.get(0);
        String url = thirdSystem.getUrl();
        LOGGER.info("MEAO url:{}", url);
        if (StringUtils.isBlank(url) || !HttpClientUtil.checkUrl(url)) {
            String msg = "create vm  image  failed. the url of selected MEAO system is invalid";
            updateActionError(actionStatus, msg);
            return false;
        }
        String imageId = sentCreateImageRequestToMeao(url, mepHost, imageName);
        if (null == imageId) {
            String msg = "create vm  image  failed. the imageId returned from meao is empty";
            updateActionError(actionStatus, msg);
            return false;
        }
        String msg = "create vm  image request sent to meao success. imageId is: " + imageId;
        updateActionProgress(actionStatus, 30, msg);
        //Save  imageId to ImageExportInfo.
        boolean updateRes = saveImageIdToImageExportInfo(imageId);
        if (!updateRes) {
            updateActionError(actionStatus, "Update ImageId To image export info failed.");
            return false;
        }
        getContext().addParameter(IContextParameter.PARAM_IMAGE_INSTANCE_ID, imageId);
        //query image status
        EnumCreateImageStatus createImageStatus = queryCreatingImageStatus(url, mepHost, imageId);
        if (!"SUCCESS".equals(createImageStatus.name())) {
            String imageErrorLog = "Query export image status failed, the result is: " + createImageStatus.name();
            updateActionError(actionStatus, imageErrorLog);
            modifyImageExportInfo(EnumImageExportStatus.FAILED, imageErrorLog);
            sendDeleteVmImageToLcm(mepHost);
            return false;
        }
        actionStatus.setStatus(EnumActionStatus.SUCCESS);
        updateActionProgress(actionStatus, 100, "Query export image status success.");
        modifyImageExportInfo(EnumImageExportStatus.IMAGE_QUERYING, "Query export image status success.");
        return true;

    }

    private Boolean saveImageIdToImageExportInfo(String imageId) {
        String vmId = (String) getContext().getParameter(IContextParameter.PARAM_VM_ID);
        ImageExportInfo imageExportInfo = vmAppOperationService.getImageExportInfo(vmId);
        imageExportInfo.setImageInstanceId(imageId);
        imageExportInfo.setStatus(EnumImageExportStatus.IMAGE_CREATING);
        vmAppOperationService.modifyExportInfo(vmId, imageExportInfo);
        return true;
    }

    private Boolean modifyImageExportInfo(EnumImageExportStatus status, String log) {
        String vmId = (String) getContext().getParameter(IContextParameter.PARAM_VM_ID);
        ImageExportInfo imageExportInfo = vmAppOperationService.getImageExportInfo(vmId);
        imageExportInfo.setStatus(status);
        imageExportInfo.setLog(log);
        vmAppOperationService.modifyExportInfo(vmId, imageExportInfo);
        return true;
    }

    private String sentCreateImageRequestToLcm(MepHost mepHost, String imageName, LcmLog lcmLog) {
        String vmInstanceId = (String) getContext().getParameter(IContextParameter.PARAM_VM_INSTANCE_ID);
        String basePath = HttpClientUtil
            .getUrlPrefix(mepHost.getLcmProtocol(), mepHost.getLcmIp(), mepHost.getLcmPort());
        String imageResult = HttpClientUtil
            .vmInstantiateImage(basePath, getContext().getUserId(), getContext().getToken(), vmInstanceId,
                mepHost.getMecHostIp(), imageName, lcmLog);
        LOGGER.info("import image result: {}", imageResult);
        if (StringUtils.isEmpty(imageResult)) {
            return null;
        }
        JsonObject jsonObject = JsonParser.parseString(imageResult).getAsJsonObject();
        JsonElement imageId = jsonObject.get("data");
        return imageId.getAsString();
    }

    private String sentCreateImageRequestToMeao(String url, MepHost mepHost, String imageName) {
        String vmInstanceId = (String) getContext().getParameter(IContextParameter.PARAM_VM_INSTANCE_ID);
        String imageResult = HttpClientUtil.createImage(vmInstanceId, url, mepHost, imageName);
        LOGGER.info("create image result: {}", imageResult);
        if (StringUtils.isEmpty(imageResult)) {
            return null;
        }
        JsonObject jsonObject = JsonParser.parseString(imageResult).getAsJsonObject();
        JsonElement imageId = jsonObject.get("imageId");
        return imageId.getAsString();
    }

    private void sendDeleteVmImageToLcm(MepHost mepHost) {
        String imageId = (String) getContext().getParameter(IContextParameter.PARAM_IMAGE_INSTANCE_ID);
        String basePath = HttpClientUtil
            .getUrlPrefix(mepHost.getLcmProtocol(), mepHost.getLcmIp(), mepHost.getLcmPort());
        HttpClientUtil.deleteVmImage(basePath, getContext().getUserId(), mepHost.getMecHostIp(), imageId,
            getContext().getToken());
    }

    private EnumCreateImageStatus queryCreatingImageStatus(String url, MepHost mepHost, String imageId) {
        long waitingTime = 0;
        long beginTime = System.currentTimeMillis();
        while (waitingTime < MEAO_TIME_OUT) {
            String resBody = HttpClientUtil.queryImageStatus(url, mepHost, imageId);
            if (StringUtils.isBlank(resBody)) {
                LOGGER.error("query image status response body is empty");
                return EnumCreateImageStatus.ERROR;
            }
            JsonObject jsonObject = JsonParser.parseString(resBody).getAsJsonObject();
            String status = jsonObject.get("status").getAsString();
            LOGGER.info("query image status: {}", status);

            if (status.equals("4")) {
                LOGGER.info("The image is created successfully");
                String downloadUrl = url + String
                    .format(Consts.MEAO_DOWNLOAD_IMAGE_URL, imageId, mepHost.getMecHostIp());
                saveImageNameAndUrl(downloadUrl, jsonObject.get("name").getAsString(),
                    jsonObject.get("disk_format").getAsString());
                return EnumCreateImageStatus.SUCCESS;
            }

            if (status.equals("5")) {
                LOGGER.info("The image does not exist, may be deleted after being uploaded");
                return EnumCreateImageStatus.DOES_NOT_EXIST;
            }

            try {
                Thread.sleep(INTERVAL);
            } catch (InterruptedException e) {
                LOGGER.error("query image status sleep failed.");
                Thread.currentThread().interrupt();
            }

            long endTime = System.currentTimeMillis();
            waitingTime = endTime - beginTime;

        }
        LOGGER.info("timeDiff:{} minutes", waitingTime / (60 * 1000));
        LOGGER.warn("Image creation timed out");
        return EnumCreateImageStatus.TIMEOUT;
    }

    private EnumExportImageStatus queryImageStatus(MepHost mepHost, String imageId) {
        int waitingTime = 0;
        String basePath = HttpClientUtil
            .getUrlPrefix(mepHost.getLcmProtocol(), mepHost.getLcmIp(), mepHost.getLcmPort());
        while (waitingTime < TIMEOUT) {
            String workStatus = HttpClientUtil
                .getImageStatus(basePath, mepHost.getMecHostIp(), getContext().getUserId(), imageId,
                    getContext().getToken());
            LOGGER.info("export image result: {}", workStatus);
            if (workStatus == null) {
                // compare time between now and deployDate
                return EnumExportImageStatus.EXPORT_IMAGE_STATUS_ERROR;
            }
            Type vmInfoType = new TypeToken<MepmVmImageInfo>() { }.getType();
            MepmVmImageInfo mepmVmImageInfo = gson.fromJson(workStatus, vmInfoType);
            if (mepmVmImageInfo.getCompressTaskStatus()
                .equals(EnumExportImageStatus.COMPRESS_IMAGE_STATUS_SUCCESS.toString())) {
                saveImageNameAndUrl(mepmVmImageInfo.getResourceUrl(), mepmVmImageInfo.getImageName(), null);
                return EnumExportImageStatus.COMPRESS_IMAGE_STATUS_SUCCESS;
            }
            if (mepmVmImageInfo.getStatus().equals(EnumExportImageStatus.EXPORT_IMAGE_STATUS_FAILED.toString())
                || mepmVmImageInfo.getCompressTaskStatus()
                .equals(EnumExportImageStatus.COMPRESS_IMAGE_STATUS_FAILURE.toString())) {
                return EnumExportImageStatus.COMPRESS_IMAGE_STATUS_FAILURE;
            }
            try {
                Thread.sleep(INTERVAL);
                waitingTime += INTERVAL;
            } catch (InterruptedException e) {
                LOGGER.error("export image sleep failed.");
                Thread.currentThread().interrupt();
            }

        }
        return EnumExportImageStatus.EXPORT_IMAGE_STATUS_TIMEOUT;
    }

    private boolean saveImageNameAndUrl(String url, String imageName, String format) {
        String vmId = (String) getContext().getParameter(IContextParameter.PARAM_VM_ID);
        ImageExportInfo imageExportInfo = vmAppOperationService.getImageExportInfo(vmId);
        if (StringUtils.isNotBlank(format)) {
            imageExportInfo.setFormat(format);
        }
        imageExportInfo.setName(imageName);
        imageExportInfo.setDownloadUrl(url);
        vmAppOperationService.modifyExportInfo(vmId, imageExportInfo);
        return true;
    }

}
