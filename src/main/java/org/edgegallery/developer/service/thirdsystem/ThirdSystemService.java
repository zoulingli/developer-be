package org.edgegallery.developer.service.thirdsystem;

import java.util.List;
import org.edgegallery.developer.model.meao.ThirdSystem;

public interface ThirdSystemService {

     List<ThirdSystem> getMeaoSystems();
}
