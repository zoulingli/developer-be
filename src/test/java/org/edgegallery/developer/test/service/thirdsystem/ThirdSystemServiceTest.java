/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.test.service.thirdsystem;

import java.util.List;
import mockit.Mock;
import mockit.MockUp;
import org.edgegallery.developer.model.meao.ThirdSystem;
import org.edgegallery.developer.service.thirdsystem.ThirdSystemService;
import org.edgegallery.developer.test.DeveloperApplicationTests;
import org.edgegallery.developer.util.HttpClientUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = DeveloperApplicationTests.class)
@RunWith(SpringRunner.class)
public class ThirdSystemServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThirdSystemServiceTest.class);

    @Autowired
    private ThirdSystemService thirdSystemService;

    private MockHttpServletRequest request;

    @Before
    public void setUp() {
        request = new MockHttpServletRequest();
        request.setCharacterEncoding("UTF-8");
    }

    @Test
    public void testGetAllMeaoSystemsSuccess() {
        MockUp mockup = new MockUp<HttpClientUtil>() {
            @Mock
            public List<ThirdSystem> getThirdSystems() {
                {
                    return null;
                }
            }
        };
        List<ThirdSystem> res = thirdSystemService.getMeaoSystems();
        Assert.assertEquals(0, res.size());
    }

}
