/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.test.service.application;

import java.util.HashMap;
import java.util.Map;
import org.edgegallery.developer.model.operation.ActionStatus;
import org.edgegallery.developer.model.operation.EnumActionStatus;
import org.edgegallery.developer.model.operation.EnumOperationObjectType;
import org.edgegallery.developer.model.operation.OperationStatus;
import org.edgegallery.developer.service.application.OperationStatusService;
import org.edgegallery.developer.service.application.common.ActionProgressRange;
import org.edgegallery.developer.test.DeveloperApplicationTests;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = DeveloperApplicationTests.class)
@RunWith(SpringRunner.class)
public class OperationStatusServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(OperationStatusServiceTest.class);

    private static final String OPERATION_STATUS_ID = "3ac35b6b-fa65-4268-894c-8c5aaa56ac3g";

    @Autowired
    private OperationStatusService operationStatusService;

    @Before
    public void setUp() {
    }

    @Test
    public void testGetOperationStatusByIdSuccess() {
        OperationStatus status = operationStatusService.getOperationStatusById(OPERATION_STATUS_ID);
        Assert.assertNotNull(status);
    }

    @Test
    public void testGetOperationStatusByIdFailed() {
        OperationStatus status = operationStatusService.getOperationStatusById("TestIdNotExist");
        Assert.assertNull(status);
    }

    @Test
    public void testCreateOperationStatusSuccess() {
        OperationStatus operationStatus = new OperationStatus();
        operationStatus.setId("3ac35b6b-fa65-4268-894c-8c5aaa56ac3t");
        operationStatus.setObjectType(EnumOperationObjectType.APPLICATION);
        operationStatus.setObjectId("testAppId");
        operationStatus.setObjectName("testApplication");
        operationStatus.setErrorMsg("");
        operationStatus.setUserName("testUser");
        operationStatus.setStatus(EnumActionStatus.ONGOING);
        operationStatus.setOperationName("Create");
        operationStatus.setProgress(50);
        Boolean result = operationStatusService.createOperationStatus(operationStatus);
        Assert.assertTrue(result);
    }

    @Test
    public void testModifyOperationStatusSuccess() {
        OperationStatus operationStatus = new OperationStatus();
        operationStatus.setId(OPERATION_STATUS_ID);
        operationStatus.setObjectType(EnumOperationObjectType.APPLICATION);
        operationStatus.setObjectId("testAppId");
        operationStatus.setObjectName("testApplication");
        operationStatus.setErrorMsg("Application create failed.");
        operationStatus.setUserName("testUser");
        operationStatus.setStatus(EnumActionStatus.FAILED);
        operationStatus.setOperationName("Create");
        operationStatus.setProgress(50);
        Boolean result = operationStatusService.modifyOperationStatus(operationStatus);
        Assert.assertTrue(result);
    }

    @Test
    public void testGetOperationCountByObjectTypeSuccess() {
        int count = operationStatusService.getOperationCountByObjectType(
            EnumOperationObjectType.APPLICATION.toString());
        Assert.assertTrue(count != 0);
    }

    @Test
    public void testGetOperationCountByObjectTypeNoResult() {
        int count = operationStatusService.getOperationCountByObjectType("InvalidObjectType");
        Assert.assertTrue(count == 0);
    }

    @Test
    public void testAddActionStatusWithUpdateOperationStatusSuccess() {
        ActionStatus actionStatus = new ActionStatus();
        actionStatus.setId("testActionId");
        actionStatus.setActionName("BuildAppPackage");
        actionStatus.setObjectId("testObjectId");
        actionStatus.setStatus(EnumActionStatus.ONGOING);
        actionStatus.setStatusLog("Start to build app package");
        actionStatus.setObjectType(EnumOperationObjectType.APPLICATION);
        Map<String, ActionProgressRange> actionProgressRangeMap = new HashMap<String, ActionProgressRange>();
        actionProgressRangeMap.put("BuildAppPackage", new ActionProgressRange(0, 20));
        actionProgressRangeMap.put("DistributeAppPackage", new ActionProgressRange(20, 50));
        actionProgressRangeMap.put("InstantiateApp", new ActionProgressRange(50, 100));
        int res = operationStatusService.addActionStatusWithUpdateOperationStatus(OPERATION_STATUS_ID, actionStatus,
            actionProgressRangeMap);
        Assert.assertTrue(res == 1);
    }

    @Test
    public void testUpdateActionStatusWithUpdateOperationStatusSuccess() {
        ActionStatus actionStatus = new ActionStatus();
        actionStatus.setId("testActionIdForUpdate");
        actionStatus.setActionName("BuildAppPackage");
        actionStatus.setObjectId("testObjectId");
        actionStatus.setStatus(EnumActionStatus.ONGOING);
        actionStatus.setProgress(50);
        actionStatus.setStatusLog("Start to build app package");
        actionStatus.setObjectType(EnumOperationObjectType.APPLICATION);
        Map<String, ActionProgressRange> actionProgressRangeMap = new HashMap<String, ActionProgressRange>();
        actionProgressRangeMap.put("BuildAppPackage", new ActionProgressRange(0, 20));
        actionProgressRangeMap.put("DistributeAppPackage", new ActionProgressRange(20, 50));
        actionProgressRangeMap.put("InstantiateApp", new ActionProgressRange(50, 100));
        operationStatusService.addActionStatusWithUpdateOperationStatus(OPERATION_STATUS_ID, actionStatus,
            actionProgressRangeMap);
        actionStatus.setStatus(EnumActionStatus.SUCCESS);
        actionStatus.setProgress(100);
        int res = operationStatusService.updateActionStatusWithUpdateOperationStatus(OPERATION_STATUS_ID, actionStatus,
            actionProgressRangeMap);
        Assert.assertTrue(res == 1);
    }
}
