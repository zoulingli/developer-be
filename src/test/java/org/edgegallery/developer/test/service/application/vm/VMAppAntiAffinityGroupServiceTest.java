/*
 *    Copyright 2022 Huawei Technologies Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.edgegallery.developer.test.service.application.vm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.edgegallery.developer.exception.DeveloperException;
import org.edgegallery.developer.model.application.vm.AntiAffinityGroup;
import org.edgegallery.developer.service.application.vm.VMAppAntiAffinityGroupService;
import org.edgegallery.developer.test.DeveloperApplicationTests;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = DeveloperApplicationTests.class)
@RunWith(SpringRunner.class)
public class VMAppAntiAffinityGroupServiceTest {

    private final String PRESET_APPLICATION1_ID = "4cbbab9d-c48f-4adb-ae82-d1816d8edd7c";

    private final String PRESET_APPLICATION2_ID = "3f11715f-b59e-4c23-965b-b7f9c34c20d1";

    private final String PRESET_VM1_ID = "6a75a2bd-9811-432f-bbe8-2813aa97d757";

    private final String PRESET_VM2_ID = "6a75a2bd-9811-432f-bbe8-2813aa97d758";

    private final String PRESET_ANTIAFFINITYGROUP1_ID = "3ac35b6b-fa65-4268-894c-8c53ec56ac3d";

    private final String PRESET_ANTIAFFINITYGROUP2_ID = "3ac35b6b-fa65-4268-894c-8c53ec56ac3e";

    private final String PRESET_ANTIAFFINITYGROUP3_ID = "3ac35b6b-fa65-4268-894c-8c53ec56ac3f";

    private final String PRESET_ANTIAFFINITYGROUP1_NAME = "antiaffinitygroup1";

    private final String PRESET_ANTIAFFINITYGROUP3_NAME = "antiaffinitygroup3";

    private final String PRESET_ANTIAFFINITYGROUP6_NAME = "antiaffinitygroup6";

    private static final Logger LOGGER = LoggerFactory.getLogger(VMAppAntiAffinityGroupServiceTest.class);

    @Autowired
    private VMAppAntiAffinityGroupService vmAppAntiAffinityGroupService;

    @Test
    public void testCreateAntiAffinityGroupSuccess() {
        AntiAffinityGroup antiAffinityGroup = new AntiAffinityGroup();
        antiAffinityGroup.setId("test_antiaffinitygroup_id");
        antiAffinityGroup.setName("test_antiaffinitygroup_name");
        antiAffinityGroup.setDescription("This is test anti affinity group.");
        antiAffinityGroup.setVmIdList(Collections.singletonList("6a75a2bd-9811-432f-bbe8-2813aa97d757"));
        AntiAffinityGroup antAffinityGroupCreated = vmAppAntiAffinityGroupService.createAntiAffinityGroup(PRESET_APPLICATION1_ID, antiAffinityGroup);
        Assert.assertEquals("test_antiaffinitygroup_name", antAffinityGroupCreated.getName());
    }

    @Test
    public void testCreateAntiAffinityGroupWithExistName() {
        AntiAffinityGroup antiAffinityGroup = new AntiAffinityGroup();
        antiAffinityGroup.setId("test_antiaffinitygroup_id");
        antiAffinityGroup.setName(PRESET_ANTIAFFINITYGROUP1_NAME);
        antiAffinityGroup.setDescription("This is test anti affinity group.");
        antiAffinityGroup.setVmIdList(Collections.singletonList("6a75a2bd-9811-432f-bbe8-2813aa97d757"));
        try {
            vmAppAntiAffinityGroupService.createAntiAffinityGroup(PRESET_APPLICATION1_ID, antiAffinityGroup);
        } catch (DeveloperException e) {
            Assert.assertEquals("Name already exists and cannot be created.", e.getMessage());
        }
    }

    @Test
    public void testCreateAntiAffinityGroupWithUnexistVm() {
        AntiAffinityGroup antiAffinityGroup = new AntiAffinityGroup();
        antiAffinityGroup.setId("test_antiaffinitygroup_id");
        antiAffinityGroup.setName("test_antiaffinitygroup_name");
        antiAffinityGroup.setDescription("This is test anti affinity group.");
        antiAffinityGroup.setVmIdList(Collections.singletonList("6a75a2bd-9811-432f-bbe8-2813aa97d756"));
        try {
            vmAppAntiAffinityGroupService.createAntiAffinityGroup(PRESET_APPLICATION1_ID, antiAffinityGroup);
        } catch (DeveloperException e) {
            Assert.assertEquals("VM does not exist.", e.getMessage());
        }

    }

    @Test
    public void testGetAllAntiAffinityGroupsSuccess() {
        List<AntiAffinityGroup> antiAffinityGroups = vmAppAntiAffinityGroupService.getAllAntiAffinityGroups(PRESET_APPLICATION1_ID, PRESET_VM1_ID);
        Assert.assertNotNull(antiAffinityGroups);
    }

    @Test
    public void testGetAntiAffinityGroupSuccess() {
        AntiAffinityGroup antiAffinityGroup = vmAppAntiAffinityGroupService.getAntiAffinityGroup(PRESET_APPLICATION1_ID, PRESET_ANTIAFFINITYGROUP1_ID);
        Assert.assertNotNull(antiAffinityGroup);
    }

    @Test
    public void testModifyAntiAffinityGroupSuccess() {
        AntiAffinityGroup antiAffinityGroup = vmAppAntiAffinityGroupService.getAntiAffinityGroup(PRESET_APPLICATION1_ID, PRESET_ANTIAFFINITYGROUP1_ID);
        Assert.assertEquals(PRESET_ANTIAFFINITYGROUP1_NAME, antiAffinityGroup.getName());
        List<String> vmIdList1 = new ArrayList<>();
        vmIdList1.add("6a75a2bd-9811-432f-bbe8-2813aa97d757");
        Assert.assertEquals(vmIdList1, antiAffinityGroup.getVmIdList());
        antiAffinityGroup.setName("antiaffinitygroup6");
        antiAffinityGroup.setVmIdList(Collections.singletonList("6a75a2bd-9811-432f-bbe8-2813aa97d758"));
        AntiAffinityGroup antiAffinityGroupModified = vmAppAntiAffinityGroupService.modifyAntiAffinityGroup(PRESET_APPLICATION1_ID, PRESET_ANTIAFFINITYGROUP1_ID, antiAffinityGroup);
        Assert.assertEquals(PRESET_ANTIAFFINITYGROUP6_NAME, antiAffinityGroupModified.getName());
        Assert.assertEquals(Collections.singletonList(PRESET_VM2_ID), antiAffinityGroupModified.getVmIdList());
    }

    @Test
    public void testModifyAntiAffinityGroupWithExistName() {
        AntiAffinityGroup antiAffinityGroup = vmAppAntiAffinityGroupService.getAntiAffinityGroup(PRESET_APPLICATION1_ID, PRESET_ANTIAFFINITYGROUP3_ID);
        Assert.assertEquals(PRESET_ANTIAFFINITYGROUP3_NAME, antiAffinityGroup.getName());
        antiAffinityGroup.setName("antiaffinitygroup2");
        try {
            vmAppAntiAffinityGroupService.modifyAntiAffinityGroup(PRESET_APPLICATION1_ID, PRESET_ANTIAFFINITYGROUP1_ID, antiAffinityGroup);
        } catch (DeveloperException e) {
            Assert.assertEquals("Name already exists and cannot be modified.", e.getMessage());
        }
    }

    @Test
    public void testModifyAntiAffinityGroupWithUnexistVm() {
        AntiAffinityGroup antiAffinityGroup = vmAppAntiAffinityGroupService.getAntiAffinityGroup(PRESET_APPLICATION1_ID, PRESET_ANTIAFFINITYGROUP1_ID);
        List<String> vmIdList1 = new ArrayList<>();
        vmIdList1.add("6a75a2bd-9811-432f-bbe8-2813aa97d757");
        Assert.assertEquals(vmIdList1, antiAffinityGroup.getVmIdList());
        antiAffinityGroup.setVmIdList(Collections.singletonList("6a75a2bd-9811-432f-bbe8-2813aa97d756"));
        try {
            vmAppAntiAffinityGroupService.modifyAntiAffinityGroup(PRESET_APPLICATION1_ID, PRESET_ANTIAFFINITYGROUP1_ID, antiAffinityGroup);
        } catch (DeveloperException e) {
            Assert.assertEquals("VM does not exist.", e.getMessage());
        }
    }

    @Test
    public void testDeleteAntiAffinityGroupSuccess() {
        boolean res = vmAppAntiAffinityGroupService.deleteAntiAffinityGroup(PRESET_APPLICATION1_ID, PRESET_ANTIAFFINITYGROUP2_ID);
        Assert.assertTrue(res);
    }

    @Test
    public void testDeleteAntiAffinityGroupByAppIdSuccess() {
        boolean res = vmAppAntiAffinityGroupService.deleteAntiAffinityGroupByAppId(PRESET_APPLICATION2_ID);
        Assert.assertTrue(res);
    }
}
