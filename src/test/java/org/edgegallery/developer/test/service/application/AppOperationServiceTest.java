/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.test.service.application;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import mockit.Mock;
import mockit.MockUp;
import org.edgegallery.developer.exception.EntityNotFoundException;
import org.edgegallery.developer.filter.security.AccessUserUtil;
import org.edgegallery.developer.model.application.EnumAppClass;
import org.edgegallery.developer.model.application.EnumApplicationType;
import org.edgegallery.developer.model.application.vm.EnumVMStatus;
import org.edgegallery.developer.model.application.vm.VMApplication;
import org.edgegallery.developer.model.application.vm.VirtualMachine;
import org.edgegallery.developer.model.apppackage.AppPackage;
import org.edgegallery.developer.model.atp.AtpTest;
import org.edgegallery.developer.model.common.User;
import org.edgegallery.developer.model.restful.SelectMepHostReq;
import org.edgegallery.developer.model.uploadfile.UploadFile;
import org.edgegallery.developer.service.application.AppOperationService;
import org.edgegallery.developer.service.application.ApplicationService;
import org.edgegallery.developer.service.application.factory.AppOperationServiceFactory;
import org.edgegallery.developer.service.application.vm.VMAppNetworkService;
import org.edgegallery.developer.service.application.vm.VMAppOperationService;
import org.edgegallery.developer.service.application.vm.VMAppVmService;
import org.edgegallery.developer.service.uploadfile.UploadFileService;
import org.edgegallery.developer.test.DeveloperApplicationTests;
import org.edgegallery.developer.util.AtpUtil;
import org.edgegallery.developer.util.HttpClientUtil;
import org.edgegallery.developer.util.SpringContextUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

@SpringBootTest(classes = DeveloperApplicationTests.class)
@RunWith(SpringRunner.class)
public class AppOperationServiceTest extends AbstractJUnit4SpringContextTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppOperationServiceTest.class);

    @Autowired
    private AppOperationServiceFactory appOperationServiceFactory;

    @Autowired
    private UploadFileService uploadFileService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private VMAppVmService vmAppVmService;

    @Autowired
    private VMAppNetworkService vmAppNetworkService;

    @Autowired
    private VMAppOperationService vmAppOperationService;

    private User user;

    Gson gson = new Gson();

    private static final String CONTAINER_APPLICATION_ID = "6a75a2bd-9811-432f-bbe8-2813aa97d364";

    private static final String VM_APPLICATION_ID = "4cbbab9d-c48f-4adb-ae82-d1816d8edd7b";

    private static final String VM_APPLICATION_WITH_INSTANTIATE_INFO_ID = "3f11715f-b59e-4c23-965b-b7f9c34c2033";

    private static final String ATP_TEST_ID = "6a75a2bd-1111-432f-bbe8-2813aa97d366";

    @Before
    public void setUp() {
        user = new User("testId", "testUser", "testAuth", "testToken");
        SpringContextUtil.setApplicationContext(applicationContext);
    }

    @Test
    public void testCleanContainerEnvSuccess() {
        AppOperationService appOperationService = appOperationServiceFactory
            .getAppOperationService(CONTAINER_APPLICATION_ID);
        boolean res = appOperationService.cleanEnv(CONTAINER_APPLICATION_ID, user);
        Assert.assertTrue(res);
    }

    @Test
    public void testCleanVMWithoutInstantiateInfoEnvSuccess() {
        AppOperationService appOperationService = appOperationServiceFactory.getAppOperationService(VM_APPLICATION_ID);
        boolean res = appOperationService.cleanEnv(VM_APPLICATION_ID, user);
        Assert.assertTrue(res);
    }

    @Test
    public void testCleanVMWithInstantiateInfoEnvSuccess() {
        MockUp mockup = mockLcmReturnInfo();
        AppOperationService appOperationService = appOperationServiceFactory
            .getAppOperationService(VM_APPLICATION_WITH_INSTANTIATE_INFO_ID);
        boolean res = appOperationService.cleanEnv(VM_APPLICATION_WITH_INSTANTIATE_INFO_ID, user);
        Assert.assertTrue(res);
        mockup.tearDown();
    }

    @Test
    public void testGenerateVMAppPackageSuccess() throws IOException {
        VMApplication vmApplication = createVmApplication();
        AppOperationService appOperationService = appOperationServiceFactory
            .getAppOperationService(vmApplication.getId());
        AppPackage appPackage = appOperationService.generatePackage(vmApplication.getId());
        Assert.assertNotNull(appPackage);
    }

    @Test
    public void testCreateAtpTestSuccess() {
        MockUp mockup = new MockUp<AtpUtil>() {
            @Mock
            public String sendCreateTask2Atp(String filePath, String token) {
                Map<String, String> result = new HashMap<String, String>();
                result.put("id", "6a75a2bd-1111-432f-bbe8-2813aa97d375");
                result.put("status", "created");
                result.put("createTime", "2021");
                result.put("appName", "appName");
                return gson.toJson(result).toString();
            }
        };
        AppOperationService appOperationService = appOperationServiceFactory.getAppOperationService(VM_APPLICATION_ID);
        boolean res = appOperationService.createAtpTest(VM_APPLICATION_ID, user);
        Assert.assertTrue(res);
        mockup.tearDown();
    }

    @Test
    public void testSelectMepHostSuccess() {
        SelectMepHostReq selectMepHostReq = new SelectMepHostReq();
        selectMepHostReq.setMepHostId("testMepHostId");
        AppOperationService appOperationService = appOperationServiceFactory.getAppOperationService(VM_APPLICATION_ID);
        boolean res = appOperationService.selectMepHost(VM_APPLICATION_ID, selectMepHostReq);
        Assert.assertTrue(res);
    }

    @Test
    public void testGetAtpTestsSuccess() {
        AppOperationService appOperationService = appOperationServiceFactory.getAppOperationService(VM_APPLICATION_ID);
        List<AtpTest> tests = appOperationService.getAtpTests(VM_APPLICATION_ID);
        Assert.assertNotNull(tests);
    }

    @Test
    public void testGetAtpTestByIdSuccess() {
        AppOperationService appOperationService = appOperationServiceFactory.getAppOperationService(VM_APPLICATION_ID);
        AtpTest atpTest = appOperationService.getAtpTestById(ATP_TEST_ID);
        Assert.assertNotNull(atpTest);
    }

    private MockUp mockLcmReturnInfo() {
        return new MockUp<HttpClientUtil>() {

            @Mock
            public boolean terminateAppInstance(String basePath, String appInstanceId, String userId, String token) {
                return true;
            }

            @Mock
            public boolean deleteHost(String basePath, String userId, String token, String pkgId, String hostIp) {
                return true;
            }

            @Mock
            public boolean deletePkg(String basePath, String userId, String token, String pkgId) {
                return true;
            }
        };
    }

    private VMApplication createVmApplication() throws IOException {
        AccessUserUtil.setUser("d59d459c-f07e-4a44-a4e6-989752038c06", "admin");
        VMApplication application = new VMApplication();
        application.setId(UUID.randomUUID().toString());
        application.setName("testVmApp");
        application.setDescription("test vm app");
        application.setVersion("v1.0");
        application.setProvider("EdgeGallery");
        application.setArchitecture("ARM");
        application.setAppClass(EnumAppClass.VM);
        application.setType("Video Application");
        application.setIndustry("Smart Park");
        MultipartFile uploadFile = new MockMultipartFile("test-icon.png", "test-icon.png", null,
            ApplicationServiceTest.class.getClassLoader().getResourceAsStream("testdata/test-icon.png"));
        UploadFile result = uploadFileService.uploadFile("b27d72b5-93a6-4db4-7777-7ec502331ade", "icon", uploadFile);
        application.setIconFileId(result.getFileId());
        application.setAppCreateType(EnumApplicationType.DEVELOP);
        application.setCreateTime(String.valueOf(new Date().getTime()));
        applicationService.createApplication(application);
        //vm app set VirtualMachine list
        VirtualMachine vm = new VirtualMachine();
        // id, app_id, name, flavor_id, image_id, user_data, status, area_zone, flavor_extra_specs
        List<VirtualMachine> vmList = new ArrayList<>();
        vm.setId(UUID.randomUUID().toString());
        vm.setName("test-vm1");
        vm.setFlavorId("96f9c44c-4d01-4da6-84dd-9f3564fe2b44");
        vm.setImageId(1);
        vm.setUserData("user data");
        vm.setStatus(EnumVMStatus.NOT_DEPLOY);
        vm.setAreaZone("az1");
        vm.setFlavorExtraSpecs("");
        vmList.add(vm);
        vmAppVmService.createVm(application.getId(), vm);
        application.setVmList(vmList);

        return application;
    }

    @Test
    public void testDownloadExportImageBad1() throws IOException {
        try {
            byte[] data = vmAppOperationService.downloadGeneratedImage("appId", "vmId");
        } catch (EntityNotFoundException e) {
            Assert.assertEquals("vm is not exit.", e.getMessage());
        }
    }

    @Test
    public void testDownloadExportImageBad2() throws IOException {
        byte[] data = vmAppOperationService
            .downloadGeneratedImage("4cbbab9d-c48f-4adb-ae82-d1816d8edd7c", "6a75a2bd-9811-432f-bbe8-2813aa97d757");
        Assert.assertEquals(0, data.length);
    }
}
