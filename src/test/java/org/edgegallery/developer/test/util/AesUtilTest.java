/*
 * Copyright 2022 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.test.util;

import org.edgegallery.developer.util.AesUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = AesUtilTest.class)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class AesUtilTest {

    private static final String CLIENT_ID = "test_client_id";


    @Test
    public void testEncode() {
        String data = "test_data";
        String encodeData = AesUtil.encode(CLIENT_ID, data);
        Assert.assertEquals("pqoSvofQ/lpOR7aLUf/YHQ==", encodeData);
    }

    @Test
    public void testEncodeFailed() {
        String encodeData = AesUtil.encode(CLIENT_ID, null);
        Assert.assertNull(encodeData);
    }

    @Test
    public void testDecode() {
        String encodeData = "pqoSvofQ/lpOR7aLUf/YHQ==";
        String data = AesUtil.decode(CLIENT_ID, encodeData);
        Assert.assertEquals("test_data", data);
    }

    @Test
    public void testDecodeFailed() {
        String encodeData = "pqoSvofQ/lpOR7aLUf/YHQ==12345";
        String data = AesUtil.decode(CLIENT_ID, encodeData);
        Assert.assertNull(data);
    }

}
