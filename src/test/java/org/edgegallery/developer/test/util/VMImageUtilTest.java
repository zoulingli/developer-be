/*
 * Copyright 2022 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.test.util;

import org.edgegallery.developer.filter.security.AccessUserUtil;
import org.edgegallery.developer.util.VMImageUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class VMImageUtilTest {

	@Test
	public void testSplitParamWithChar() {
		String testParam = "1,2,3,4,5";
		List<String> res = VMImageUtil.splitParam(testParam);
		Assert.assertEquals(5, res.size());
	}

	@Test
	public void testSplitParamWithoutChar() {
		String testParam = "12345";
		List<String> res = VMImageUtil.splitParam(testParam);
		Assert.assertEquals(1, res.size());
		Assert.assertEquals(testParam, res.get(0));
	}

	@Test
	public void testIsAdminUser() {
		AccessUserUtil.setUser("userID", "userName", "userAuth_ROLE_DEVELOPER_ADMIN", "token");
		boolean res = VMImageUtil.isAdminUser();
		Assert.assertTrue(res);
	}


	@Test
	public void testIsAdminUserReturnFalse() {
		AccessUserUtil.setUser("", "");
		boolean res = VMImageUtil.isAdminUser();
		Assert.assertFalse(res);
	}


}
