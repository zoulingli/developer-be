/*
 * Copyright 2022 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.test.controller.application.vm;

import com.google.gson.Gson;
import org.edgegallery.developer.test.DeveloperApplicationTests;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.edgegallery.developer.model.application.vm.AntiAffinityGroup;
import org.edgegallery.developer.service.application.vm.VMAppAntiAffinityGroupService;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DeveloperApplicationTests.class)
@AutoConfigureMockMvc
public class VMAppAntiAffinityGroupCtlTest {

    @MockBean
    private VMAppAntiAffinityGroupService vmAppAntiAffinityGroupService;

    @Autowired
    private MockMvc mvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @WithMockUser(roles = "DEVELOPER_ADMIN")
    public void testCreateAntiAffinityGroupSuccess() throws Exception{
        AntiAffinityGroup antiAffinityGroup = new AntiAffinityGroup();
        String url = String.format("/mec/developer/v2/applications/%s/anti-affinity-groups", UUID.randomUUID().toString());
        Mockito.when(vmAppAntiAffinityGroupService.createAntiAffinityGroup(Mockito.anyString(), Mockito.any())).thenReturn(antiAffinityGroup);
        ResultActions actions = mvc.perform(
                MockMvcRequestBuilders.post(url).with((csrf())).content(new Gson().toJson(new AntiAffinityGroup()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(200, actions.andReturn().getResponse().getStatus());
    }

    @Test
    @WithMockUser(roles = "DEVELOPER_ADMIN")
    public void testGetAllAntiAffinityGroupsSuccess() throws Exception {
        List<AntiAffinityGroup> antiAffinityGroups = new ArrayList<>();
        String url = String.format("/mec/developer/v2/applications/%s/anti-affinity-groups", UUID.randomUUID().toString());
        Mockito.when(vmAppAntiAffinityGroupService.getAllAntiAffinityGroups(Mockito.anyString(),Mockito.anyString())).thenReturn(antiAffinityGroups);
        ResultActions actions = mvc.perform(
                MockMvcRequestBuilders.get(url).with((csrf())).content(new Gson().toJson(new AntiAffinityGroup()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(200, actions.andReturn().getResponse().getStatus());
    }

    @Test
    @WithMockUser(roles = "DEVELOPER_ADMIN")
    public void testGetAntiAffinityGroupSuccess() throws Exception {
        AntiAffinityGroup antiAffinityGroup = new AntiAffinityGroup();
        String url = String.format("/mec/developer/v2/applications/%s/anti-affinity-groups/%s", UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Mockito.when(vmAppAntiAffinityGroupService.getAntiAffinityGroup(Mockito.anyString(), Mockito.anyString())).thenReturn(antiAffinityGroup);
        ResultActions actions = mvc.perform(
                MockMvcRequestBuilders.get(url).with((csrf())).content(new Gson().toJson(new AntiAffinityGroup()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(200, actions.andReturn().getResponse().getStatus());
    }

    @Test
    @WithMockUser(roles = "DEVELOPER_ADMIN")
    public void testModifyAntiAffinityGroupSuccess() throws Exception {
        AntiAffinityGroup antiAffinityGroup = new AntiAffinityGroup();
        String url = String.format("/mec/developer/v2/applications/%s/anti-affinity-groups/%s", UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Mockito.when(vmAppAntiAffinityGroupService.modifyAntiAffinityGroup(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenReturn(antiAffinityGroup);
        ResultActions actions = mvc.perform(
                MockMvcRequestBuilders.put(url).with((csrf())).content(new Gson().toJson(new AntiAffinityGroup()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(200, actions.andReturn().getResponse().getStatus());
    }

    @Test
    @WithMockUser(roles = "DEVELOPER_ADMIN")
    public void testDeleteAntiAffinityGroupSuccess() throws Exception {
        String url = String.format("/mec/developer/v2/applications/%s/anti-affinity-groups/%s", UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Mockito.when(vmAppAntiAffinityGroupService.deleteAntiAffinityGroup(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
        ResultActions actions = mvc.perform(
                MockMvcRequestBuilders.delete(url).with((csrf())).content(new Gson().toJson(new AntiAffinityGroup()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(200, actions.andReturn().getResponse().getStatus());
    }
}
